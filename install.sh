#!/bin/bash
echo "here i am: $(pwd)"
cp CustomWelcome-1.0-SNAPSHOT.jar /home/ubuntu/mc-server/plugins/CustomWelcome-1.0-SNAPSHOT.jar # copy plugin to plugin directory
curl -H "Content-type: application/json" -d '{"add_record":true, "name":"Custom Welcome 1.0","remove_script":"/home/ubuntu/mc-server/plugins/rmCustomWelcome-1.0.sh","file_0":"CustomWelcome/config.yml"}' 'http://127.0.0.1:8181' # add plugin to panel
echo "#!/bin/bash">/home/ubuntu/mc-server/plugins/rmCustomWelcome-1.0.sh
echo "rm -rf /home/ubuntu/mc-server/plugins/CustomWelcome">>/home/ubuntu/mc-server/plugins/rmCustomWelcome-1.0.sh # delete plugin folder
echo "rm /home/ubuntu/mc-server/plugins/CustomWelcome-1.0-SNAPSHOT.jar">>/home/ubuntu/mc-server/plugins/rmCustomWelcome-1.0.sh # delete plugin
echo "curl -H \"Content-type: application/json\" -d '{\"remove\":true, \"name\":\"Custom Welcome 1.0\"}' 'http://127.0.0.1:8181'">>/home/ubuntu/mc-server/plugins/rmCustomWelcome-1.0.sh # delete plugin from panel
chmod +x /home/ubuntu/mc-server/plugins/rmCustomWelcome-1.0.sh # make it executable
